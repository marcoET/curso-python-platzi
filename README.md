# Curso de Python 

[Curso de Python en Platzi](https://platzi.com/clases/python-basico/)

**Profesor** David Aroesti
[@jdaroesti](https://twitter.com/jdaroesti)

Porque aprender Pyhton:
* Tiene las comunidades mas grandes
* Tiene las mejores sintaxis, no tiene signos extraños

## Orden de operaciones

PEMDAS
* Paréntesis
* Exponentes
* Multiplicación / División
* Adición / Sustración

Valores y Tipos

## Los tipos le permiten a Python saber cuál es el resultado de aplicar determinada operación, los tipos básicos son:

**Integer <int>
* Float <float>
* String <str>
* Boolean <bool>

## Declarar variables y expresiones

Las variables nos permiten guardar valores, permitiéndonos reutilizarlos en diferentes partes del código y haciendo nuestros programas más legibles.

El valor que contiene una variable puede ser reasignado, significa que podemos asignarle diferentes valores a una misma variable.

Las variables tienen algunas limitantes, por ejemplo
* Tienen que tener un nombre significativo, es decir, que nos digan qué están haciendo.
* No podemos usar palabras reservadas del lenguaje como nombres para nuestras variables (por ejemplo class, false, none, true).

Para definir una variable escribes el nombre que quieres asignarle y con = defines el valor que va a almacenar, por ejemplo:

pi = 3.14159
radio =3
area = pi * radio**2
print(area)

**Recuerda**:
* Todos los programas de Python deben guardarse con extensión .py
* Para darle soporte a acentos en nuestros programas debemos usar la línea # -*- coding: utf-8 -*-

## Funciones
Las funciones son un concepto fundamental en programación, una función es una secuencia de comandos que realizan un computo.
En Python las funciones se definen usando la palabra reservada def y luego el nombre de la función con paréntesis y dos puntos que indican que lo que sigue son los comandos, una función debe retornar un valor, para esto se usa la palabra reservada return.

```python
def  suma(num1, num2):
    return num1 + num2
```

Para usar una función simplemente la llamamos por su nombre seguido por paréntesis con los parámetros que recibe.

```python
suma(2,3)
```

**Recuerda:**
* Si usas Python 3, debes usar la función input() para recibir datos del usuario.
* Para definir dónde comenzar el código usamos la línea Screen Shot 2017-08-17 at 4.42.46 PM.png
* Para definir un bloque dentro de la función debemos indentar con 4 espacios.
* Las funciones nos permiten ejecutar determinado código con diferentes valores.

## Funciones con parámetros

**Limites al declarar funciones**

* Los nombres no pueden comenzar con digitos
* No pueden utilizar una palabra reservada
* Las variables deben tener diferentes nombres
* Los nombres de las funciones deben ser descriptivas de lo que hacen las funciones

**Imprimir valor de variable**

Para poder imprimir el valor de una variable dentro de un string podemos hacerlo así:

'${} pasos mexicanos son ${} pesos mexicanos'.format(ammount, result)

**Declarar vs Ejecutar**

Declarar una función es escribir su estructura
Ejecutar una función es llamar la función y ejecutar su código

**Donde se puede acceder a las variables**

Scope de las variables, cada vez que una función se ejecuta se genera un contenedor donde las variables de la función van a vivir, una vez se sale de la función estas variables no van a existir.

## Estructura de condicionales en Python

Uno de las cosas más poderosas en programación es controlar el flujo de ejecución, para esto utilizamos condicionales.

Las condicionales son sentencias que devuelven un valor booleano (True, False)

Podemos utilizar:

**Operadores relacionales:**

== Es igual
!= Es diferente
> Es mayor
>= Es mayor o igual
< Es menor
<= Es menor o igual

**Operadores lógicos:**

and
or
not

## El Zen de Python
* Hermoso es mejor que feo.
* Explícito es mejor que implícito.
* Simple es mejor que complejo.
* Complejo es mejor que complicado.
* Sencillo es mejor que anidado.
* Escaso es mejor que denso.
* La legibilidad cuenta.
* Los casos especiales no son lo suficientemente especiales para romper las reglas.
* Lo práctico le gana a la pureza.
* Los errores no debe pasar en silencio.
* A menos que sean silenciados.
* En cara a la ambigüedad, rechazar la tentación de adivinar.
* Debe haber una - y preferiblemente sólo una - manera obvia de hacerlo.
* Aunque esa manera puede no ser obvia en un primer momento a menos que seas holandés.
* Ahora es mejor que nunca.
* Aunque “nunca” es a menudo mejor que “ahora mismo”.
* Si la aplicación es difícil de explicar, es una mala idea.
* Si la aplicación es fácil de explicar, puede ser una buena idea.
* Los espacios de nombres son una gran idea ¡hay que hacer más de eso!

## Comparación de strings y unicode
Los strings tienen una característica muy importante: son inmutables, esto quiere decir que no se pueden cambiar después de que se han declarado.

Si quieres modificar el texto de un string debes definir un nuevo string y modificarlo usando funciones como slice.

**Comparación de strings**

Se pueden realizar operaciones con strings, por ejemplo comparar si son iguales o mayores o menores.

**Diferencia entre ASCII y Unicode**

Los caracteres también son números, para esto existen estándares que asignan un número a cada carácter, para generar un estándar se creó el ASCII pero esta solo toma en cuenta los caracteres en inglés, para dar soporte a más lenguajes se crea UNICODE.

Python codifica en ASCII por default, para cambiarlo por UNICODE debemos colocar u antes del string.

Python 2 asume que las string esta en ASCII, por ello luego se coloca # -*- coding: utf8-8 -*-
Python 3 maneja UNICODE

## Factorial de un número con recursión

En éste video hablaremos sobre la recursión, una función está siendo recursiva cuando dentro de el bloque de instrucciones que la conforma se usa a sí misma.

El concepto puede sonar complicado pero es muy común su uso, por ejemplo cuando haces el calculo del factorial de un número lo haces con una función recursiva:

El factorial de un número es el número multiplicado por los números antes de el, por ejemplo

5! es 5*4*3*2*1

Esto se puede expresar como

```python
5*fac(4)
4*fac(3)
3*fac(2)
2*fac(1)
1*fac(0)
```

Nota importante: Cuándo estes trabajando con recursividad siempre debes pensar en el caso base, es decir debes definir el momento en el que la función dejará de llamarse a si misma, para que no hagas un loop infinito, por ejemplo en el caso del factorial terminas la ejecución cuando llegas a cero.
