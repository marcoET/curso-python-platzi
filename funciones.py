import turtle

def main():
    window = turtle.Screen()
    marco = turtle.Turtle()
    make_square(marco)
    turtle.mainloop()

def make_square(marco):
    length = int(input('Tamaño del cuadro: '))
    
    for i in range(4):
        make_line_and_turn(marco, length)

def make_line_and_turn(marco, length):
    marco.forward(length)
    marco.left(90)

# Definimos donde inicia el archivo
if __name__ == '__main__':
    main()